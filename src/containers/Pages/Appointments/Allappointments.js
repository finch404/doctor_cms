import React, { Component } from 'react'
import './appointments.css'
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { fetchEmployee } from "../../../store/actions/Employee/employee";
import { Whatsapp } from "../../Home/Whatsapp";
export class Allappointments extends Component {
    componentDidMount() {
        this.props.fetchEmployee(this.props);
    }
    // componentDidUpdate() {
    //   this.props.fetchEmployee();
    // }

    render() {
        const { employee } = this.props;
        return (
            <div>
                <div class="breadcrumb-holder">
                    <div class="container-fluid">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Tables </li>
                        </ul>
                    </div>
                </div>
                <br />
                <div className="card">
                    <div class="card-body">
                        <div>
                            <table className="table table-bordered table-responsive-sm table-responsive-md">
                                <thead>
                                    <tr>
                                        <th scope="col">Name</th>
                                       
                                        <th scope="col">Email</th>
                                        <th scope="col">Contact Number</th>
                                        <th scope="col">Date </th>
                                        <th scope="col">Reason </th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {employee ? (
                                        employee.map(function (item, id) {
                                            return (
                                                <tr key={id}>
                                                    <td scope="row">{item.empName || "NO DATA"}</td>
                                                   
                                                    <td scope="row">{item.empEmail || "NO DATA"}</td>
                                                    <td scope="row">{item.empMobile || "NO DATA"}</td>
                                                    <td scope="row">December 22, 2019</td>
                                                    <td scope="row">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </td>
                                                 
                                                    <td>
                                                        {/* <Link to={`/EditEmploye/${item.employe_id}`}> */}
                                                        {/* <NavLink to ={'/Editjob'}> */}
                                                        <span
                                                            class="tooltip-toggle"
                                                            aria-label="Edit"
                                                            tabindex="0"
                                                        >
                                                            <button
                                                                className="btn btn-success custom-edit-btn btn-sm"
                                                                // onClick={this.editfunc.bind(this, item)}
                                                                // data-toggle="modal"
                                                                // data-target=".bd-example-modal-lg"
                                                                disabled

                                                            >
                                                               <i class="fa fa-check" aria-hidden="true"></i>
                                                            </button>
                                                        </span>
                                                        {/* </NavLink> */}

                                                       

                                                    </td>
                                                </tr>
                                            );
                                        }, this)
                                    ) : (
                                            <span>Data is loading....</span>
                                        )}
                                </tbody>
                            </table>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <Whatsapp />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        employee: state.employee,
        currentUser: state.currentUser.user.id // we only need the current user's id, to check against the user id of each message
    };
}

export default connect(
    mapStateToProps,
    { fetchEmployee }
)(Allappointments);